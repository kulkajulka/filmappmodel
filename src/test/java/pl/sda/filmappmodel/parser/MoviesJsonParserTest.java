package pl.sda.filmappmodel.parser;

import org.apache.log4j.Logger;
import org.junit.Test;
import pl.sda.filmappmodel.model.Film;

import java.util.List;

import static org.apache.log4j.Logger.getLogger;
import static org.junit.Assert.*;

public class MoviesJsonParserTest {
    private static final int MOVIES_COUNT = 2295;
    private static final int TEST_MOVIES_COUNT = 3;
    private static final Logger LOGGER = getLogger(MoviesJsonParser.class);
    @Test
    public void parseManyJsonsFromFile() throws Exception {
        //given
        String fileName = "threemovies.json";
        //when
        List<Film> movies = MoviesJsonParser.parseJsonsFromFile(fileName);
        //then
        assertEquals(TEST_MOVIES_COUNT,movies.size());
        movies.forEach(LOGGER::info);
    }

    @Test
    public void parseOneJsonFromFile() throws Exception {
        // given
        String fileName = "one.json";
        //when
        Object movie = MoviesJsonParser.parseJsonsFromFile(fileName).get(0);
        assertNotNull(movie);
        //then
        assertTrue(movie instanceof Film);
        assertEquals("Sergio Leone",((Film)movie).getDirector());
        assertEquals("Once Upon a Time in the West",((Film)movie).getTitle());
        assertEquals(1968,((Film)movie).getYear());
        assertEquals("Western",((Film)movie).getGenre().getName());
        assertEquals("http://ia.media-imdb.com/images/M/MV5BMTEyODQzNDkzNjVeQTJeQWpwZ15BbWU4MDgyODk1NDEx._V1_SX300.jpg",((Film)movie).getPosterUrl());
    }

    @Test
    public void parseAllJsonFromFile() {
        // given
        String file = "movies.json";
        // when
        List<Film> movies = MoviesJsonParser.parseJsonsFromFile(file);
        // then
        movies.forEach(LOGGER::info);
        assertEquals(MOVIES_COUNT,movies.size());
    }

}