package pl.sda.filmappmodel.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static javax.validation.Validation.buildDefaultValidatorFactory;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TomatoTest {

    private Validator validator;

    @Before
    public void init() {
        ValidatorFactory validatorFactory = buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    public void validationTestInvalidUserRating() {
        //given
        Tomato tomatoUserRatingHigher = new Tomato(11.3, 300);
        Tomato tomatoUserRatingLower = new Tomato(-0.2, 300);

        //when
        Set<ConstraintViolation<Tomato>> constraintHigher = validator.validate(tomatoUserRatingHigher);
        //then
        assertFalse(constraintHigher.isEmpty());

        //when
        Set<ConstraintViolation<Tomato>> constraintLower = validator.validate(tomatoUserRatingLower);
        //then
        assertFalse(constraintLower.isEmpty());
    }

    @Test
    public void validationTestInclusiveMaxMinValue() {
        //given
        Tomato tomatoUserRatingAsMax = new Tomato(10.0, 1300);
        Tomato tomatoUserRatingAsMin = new Tomato(0.0, 1300);

        //when
        Set<ConstraintViolation<Tomato>> constraintMax = validator.validate(tomatoUserRatingAsMax);
        //then
        assertTrue(constraintMax.isEmpty());

        //when
        Set<ConstraintViolation<Tomato>> constraintMin = validator.validate(tomatoUserRatingAsMin);
        //then
        assertTrue(constraintMin.isEmpty());
    }
}