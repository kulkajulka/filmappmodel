package pl.sda.filmappmodel.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashSet;
import java.util.Set;

import static javax.validation.Validation.buildDefaultValidatorFactory;
import static org.junit.Assert.*;

public class FilmTest {

    private Validator validator;

    @Before
    public void init(){
        ValidatorFactory validatorFactory = buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    public void filmPosterUrlNull(){
        //given
        Film film =
                new Film("tytul", "director", 2011, new Genre(), null, new HashSet<Actor>(),new Tomato(), new Imdb());
        //when
        Set<ConstraintViolation<Film>> constraints = validator.validate(film);
        //then
        assertTrue(constraints.isEmpty());
    }

    @Test
    public void putInvalidData(){
        Film film =
                new Film(null, "di", 2055, new Genre(),"", new HashSet<Actor>(),new Tomato(), new Imdb());
        //when
        Set<ConstraintViolation<Film>> constraints = validator.validate(film);
        //then
        assertFalse(constraints.isEmpty());
        assertEquals(3, constraints.size());
    }

    @Test
    public void invalidUrlRegexValidation(){
        //given
        String[] invalidUrlTab = {
                "www.google.com",
                "www.fdsfsdfsdfsdf.ddd",
                "http:/ia.media-imdb.com/images/M/MV5BMTEyODQzNDkzNjVeQTJeQWpwZ15BbWU4MDgyODk1NDEx._V1_SX300.jpg",
                "ftp://ia.media-imdb.com/images/M/MV5BMTEyODQzNDkzNjVeQTJeQ'WpwZ15BbWU4MDgyODk1NDEx._V1_SX300.jpg"
        };

        Film film =
                new Film("tytul", "director", 2011, new Genre(), "plot", new HashSet<Actor>(),new Tomato() ,new Imdb());
        for (String url: invalidUrlTab) {
            film.setPosterUrl(url);
            //when
            Set<ConstraintViolation<Film>> constraintViolations = validator.validate(film);
            //then
            assertFalse(constraintViolations.isEmpty());
            assertEquals(4, invalidUrlTab.length);
        }
    }

    @Test
    public void correctUrlRegexValidation(){
        //given
        String[] correctUrlTab = {
                "http://www.google.com",
                "http://ia.media-imdb.com/images/M/MV5BMTEyODQzNDkzNjVeQTJeQWpwZ15BbWU4MDgyODk1NDEx._V1_SX300.jpg",
                "ftp://ia.media-imdb.com/images/M/MV5BMTEyODQzNDkzNjVeQTJeQWpwZ15BbWU4MDgyODk1NDEx._V1_SX300.jpg"
        };
        Film film =
                new Film("tytul", "director", 2011, new Genre(), "plot", new HashSet<Actor>(),new Tomato() ,new Imdb());
        for (String url: correctUrlTab) {
            film.setPosterUrl(url);
            //when
            Set<ConstraintViolation<Film>> constraintViolations = validator.validate(film);
            //then
            assertTrue(constraintViolations.isEmpty());
            assertEquals(3,correctUrlTab.length);
        }
    }
}