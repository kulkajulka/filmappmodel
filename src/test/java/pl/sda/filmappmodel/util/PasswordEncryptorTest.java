package pl.sda.filmappmodel.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static pl.sda.filmappmodel.util.PasswordEncryptor.checkPassword;
import static pl.sda.filmappmodel.util.PasswordEncryptor.encryptPassword;

@RunWith(JUnitParamsRunner.class)
public class PasswordEncryptorTest {

    String basePassword;

    EncryptedPair pair;

    @Before
    public void before() {
        basePassword = "ABCD";
        pair = encryptPassword(basePassword);
    }

    @Parameters({
            "ABCD, true",
            "abcd, false",
            "ĄBĆD, false",
            "ABCd, false",
            "1234, false"})
    @Test
    public void passwordHashingTestForABCD(String input, boolean valid) throws Exception {
        assertThat(checkPassword(input, pair), is(valid));
    }
}