package pl.sda.filmappmodel.parser;

import com.google.gson.*;
import pl.sda.filmappmodel.model.Genre;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GenreDeserializer implements JsonDeserializer<Genre> {
    private static final Map<String, Integer> genreMap = new HashMap<>();

    static {
        genreMap.put("Western", 1);
        genreMap.put("Comedy", 2);
        genreMap.put("Action", 3);
        genreMap.put("Crime", 4);
        genreMap.put("Drama", 5);
        genreMap.put("Musical", 6);
        genreMap.put("Thriller", 7);
        genreMap.put("Adventure", 8);
        genreMap.put("Animation", 9);
        genreMap.put("Family", 10);
        genreMap.put("Fantasy", 11);
        genreMap.put("Documentary", 12);
        genreMap.put("Sci-Fi", 13);
        genreMap.put("Romance", 14);
        genreMap.put("Biography", 15);
        genreMap.put("Mystery", 16);
        genreMap.put("Horror", 17);
        genreMap.put("Music", 18);
        genreMap.put("History", 19);
        genreMap.put("War", 20);
        genreMap.put("Short", 21);
        genreMap.put("Film-Noir", 22);
        genreMap.put("Sport", 23);
        genreMap.put("Talk-Show", 24);
        genreMap.put("News", 25);
        genreMap.put("Reality-TV", 26);
        genreMap.put("Adult", 27);
        genreMap.put("Game-Show", 28);
    }

    @Override
    public Genre deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonArray asJsonArray = json.getAsJsonArray();
        int jsonArraySize = asJsonArray.size();
        if (jsonArraySize != 0) {
            JsonElement jsonElement = asJsonArray.get(jsonArraySize - 1);
            String asString = jsonElement.getAsString();
            return new Genre(genreMap.get(asString),asString);
        }
        return null;
    }
}
