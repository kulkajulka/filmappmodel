package pl.sda.filmappmodel.parser;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
import pl.sda.filmappmodel.model.Actor;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappmodel.model.Genre;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.apache.log4j.Logger.getLogger;

public class MoviesJsonParser {
    private static final Logger LOGGER = getLogger(MoviesJsonParser.class);

    public static List<Film> parseJsonsFromFile(String filename) {
        try {
            BufferedReader reader = getResourceAsBufferedReader(filename);
            Gson gson = getGson();
            return Arrays.asList(gson.fromJson(reader, Film[].class));
        } catch (FileNotFoundException e) {
            LOGGER.info(e.getMessage());
        }
        return Collections.emptyList();
    }

    private static BufferedReader getResourceAsBufferedReader(String fileName) throws FileNotFoundException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        URL classLoaderResource = classLoader.getResource(fileName);
        if (classLoaderResource != null) {
            return new BufferedReader(new FileReader(new File(classLoaderResource.getFile())));
        }
        else {
            throw new FileNotFoundException("No such resource!");
        }
    }

    private static Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Actor.class,new ActorDeserializer())
                .registerTypeAdapter(Genre.class,new GenreDeserializer())
                .create();
    }
}
