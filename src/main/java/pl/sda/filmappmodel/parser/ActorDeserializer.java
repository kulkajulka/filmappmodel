package pl.sda.filmappmodel.parser;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import pl.sda.filmappmodel.model.Actor;

import java.lang.reflect.Type;
import java.util.Date;

public class ActorDeserializer implements JsonDeserializer<Actor>{

    @Override
    public Actor deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
        String[] name = jsonElement.getAsString().split(" ");
        return new Actor(name[0], name.length == 2 ? name[1] : "", new Date(0L),
                new Date(0L), Math.floor((Math.random() * 100)) / 10, null);
    }
}
