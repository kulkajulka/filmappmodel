package pl.sda.filmappmodel.util;

import org.bouncycastle.jcajce.provider.digest.SHA3.Digest256;
import org.bouncycastle.jcajce.provider.digest.SHA3.DigestSHA3;

import java.security.MessageDigest;
import java.security.SecureRandom;

public class PasswordEncryptor {

    private static final int seedBytes = 20;
    private static final SecureRandom rng = new SecureRandom();
    private static final DigestSHA3 sha3 = new Digest256();

    public static EncryptedPair encryptPassword(final String input) {

        byte[] salt = generateSalt();
        generateHash(input, salt);
        return getOutput(salt);
    }

    private static EncryptedPair getOutput(byte[] salt) {
        return new EncryptedPair(hashToString(sha3), salt.toString());
    }

    private static void generateHash(String input, byte[] salt) {
        sha3.update((input + salt.toString()).getBytes());
    }

    private static byte[] generateSalt() {
        return rng.generateSeed(seedBytes);
    }

    public static String hashToString(MessageDigest hash) {
        return hashToString(hash.digest());
    }

    public static String hashToString(byte[] hash) {
        StringBuilder buff = new StringBuilder();
        for (byte b : hash) {
            buff.append(String.format("%02x", b & 0xFF));
        }
        return buff.toString();
    }

    static boolean checkPassword(String contender, EncryptedPair encryptedPair) {
        sha3.update((contender.concat(encryptedPair.getSalt()).getBytes()));
        return encryptedPair.getHash().equals(hashToString(sha3));
    }
}
