package pl.sda.filmappmodel.util;

import org.springframework.data.util.Pair;

public class EncryptedPair {

    private Pair<String, String> hashWithSalt;

    EncryptedPair(String hash, String salt) {
        hashWithSalt = Pair.of(hash, salt);
    }

    public String getHash() {
        return hashWithSalt.getFirst();
    }

    public String getSalt() {
        return hashWithSalt.getSecond();
    }

    @Override
    public String toString() {
        return "hash : " + getHash() + ", salt :" + getSalt();
    }
}



