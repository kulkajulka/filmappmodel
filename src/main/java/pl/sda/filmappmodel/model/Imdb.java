package pl.sda.filmappmodel.model;

import javax.persistence.Embeddable;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;

@Embeddable
public class Imdb {

    @DecimalMax(value = "10.0")
    @DecimalMin(value = "0.0")
    private Double rating;

    @Min(0)
    private Integer votes;

    public Imdb(Double rating, Integer votes) {
        this.rating = rating;
        this.votes = votes;
    }

    public Imdb() {
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Imdb imdb = (Imdb) o;

        if (rating != null ? !rating.equals(imdb.rating) : imdb.rating != null) return false;
        return votes != null ? votes.equals(imdb.votes) : imdb.votes == null;
    }

    @Override
    public int hashCode() {
        int result = rating != null ? rating.hashCode() : 0;
        result = 31 * result + (votes != null ? votes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Imdb{" +
                "rating=" + rating +
                ", votes=" + votes +
                '}';
    }
}
