package pl.sda.filmappmodel.model;

import pl.sda.filmappmodel.util.EncryptedPair;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.AUTO;
import static pl.sda.filmappmodel.util.PasswordEncryptor.encryptPassword;

@Entity
@Table(name = "user")
public class User implements Serializable{

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "user_name", unique = true)
    private String username;

    @Transient
    private String password;

    @NotNull
    @Size(max = 100)
    private String hash;

    @NotNull
    @Size(min = 3, max = 100)
    private String salt;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "genre_preference")
    @Size(max = 50)
    private String genrePreference;

    @NotNull
    @Past
    @Column(name = "birth_date")
    private Date birthDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private UserRole role;

    @Column(name = "sign_in_counter")
    @Min(0)
    private Long signInCounter;

    public User() {
    }

    public User(String username,
                String password,
                String firstName,
                String lastName,
                String genrePreference,
                Date birthDate,
                UserRole role) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.genrePreference = genrePreference;
        this.birthDate = birthDate;
        this.role = role;
        EncryptedPair pair = encryptPassword(password);
        this.hash = pair.getHash();
        this.salt = pair.getSalt();
    }

    public User(int id,
                String username,
                String password,
                String firstName,
                String lastName,
                String genrePreference,
                Date birthDate,
                UserRole role) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.genrePreference = genrePreference;
        this.birthDate = birthDate;
        this.role = role;
        EncryptedPair pair = encryptPassword(password);
        this.hash = pair.getHash();
        this.salt = pair.getSalt();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHash() {
        return hash;
    }

    public String getSalt() {
        return salt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGenrePreference() {
        return genrePreference;
    }

    public void setGenrePreference(String genrePreference) {
        this.genrePreference = genrePreference;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Long getSignInCounter() {
        return signInCounter;
    }

    public void setSignInCounter(Long signInCounter) {
        this.signInCounter = signInCounter;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        EncryptedPair pair = encryptPassword(password);
        this.hash = pair.getHash();
        this.salt = pair.getSalt();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (username != null ? !username.equals(user.username) : user.username != null) return false;
        if (hash != null ? !hash.equals(user.hash) : user.hash != null) return false;
        if (salt != null ? !salt.equals(user.salt) : user.salt != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (genrePreference != null ? !genrePreference.equals(user.genrePreference) : user.genrePreference != null)
            return false;
        return role == user.role;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (hash != null ? hash.hashCode() : 0);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (genrePreference != null ? genrePreference.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (signInCounter != null ? signInCounter.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", hash='" + hash + '\'' +
                ", salt='" + salt + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", genrePreference='" + genrePreference + '\'' +
                ", role=" + role +
                ", signInCounter=" + signInCounter +
                '}';
    }
}
