package pl.sda.filmappmodel.model;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "film")
public class Film {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "film_id")
    private Integer id;

    @NotNull
    @Size(max = 150)
    private String title;

    @Size(max = 600)
    private String director;

    @NotNull
    @Min(1890)
    @Max(2018)
    private int year;

    @SerializedName("genres")
    @ManyToOne(targetEntity = Genre.class)
    @JoinColumn(name = "genre_id")
    private Genre genre = new Genre();

    @SerializedName("poster")
    @Pattern(regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
    @Column(name = "poster")
    private String posterUrl;

    @ManyToMany(cascade = ALL, fetch = EAGER)
    @JoinTable(name = "film_has_actor",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "actor_id"))
    @NotNull
    private Set<Actor> actors = new HashSet<>();

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "rating", column = @Column(name = "imdb_rating")),
            @AttributeOverride(name = "votes", column = @Column(name = "imdb_votes"))
    })
    private Imdb imdb;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "userRating", column = @Column(name = "tomato_rating")),
            @AttributeOverride(name = "userReviews", column = @Column(name = "tomato_reviews"))
    })
    private Tomato tomato;

    public Film() {
    }

    public Film(String title,
                String director,
                int year,
                Genre genre,
                String posterUrl,
                Set<Actor> actors,
                Tomato tomato,
                Imdb imdb) {
        this.title = title;
        this.director = director;
        this.year = year;
        this.genre = genre;
        this.posterUrl = posterUrl;
        this.actors = actors;
        this.tomato = tomato;
        this.imdb = imdb;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public Set<Actor> getActors() {
        return actors;
    }

    public void setActors(Set<Actor> actors) {
        this.actors = actors;
    }

    public Tomato getTomato() {
        return tomato;
    }

    public void setTomato(Tomato tomato) {
        this.tomato = tomato;
    }

    public Imdb getImdb() {
        return imdb;
    }

    public void setImdb(Imdb imdb) {
        this.imdb = imdb;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", director='" + director + '\'' +
                ", year=" + year +
                ", genre=" + genre +
                ", posterUrl='" + posterUrl + '\'' +
                ", actors=" + actors +
                ", imdb=" + imdb +
                ", tomato=" + tomato +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Film film = (Film) o;

        if (year != film.year) return false;
        if (title != null ? !title.equals(film.title) : film.title != null) return false;
        if (director != null ? !director.equals(film.director) : film.director != null) return false;
        if (genre != null ? !genre.equals(film.genre) : film.genre != null) return false;
        if (posterUrl != null ? !posterUrl.equals(film.posterUrl) : film.posterUrl != null) return false;
        if (imdb != null ? !imdb.equals(film.imdb) : film.imdb != null) return false;
        return tomato != null ? tomato.equals(film.tomato) : film.tomato == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (director != null ? director.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (posterUrl != null ? posterUrl.hashCode() : 0);
        result = 31 * result + (imdb != null ? imdb.hashCode() : 0);
        result = 31 * result + (tomato != null ? tomato.hashCode() : 0);
        return result;
    }
}
