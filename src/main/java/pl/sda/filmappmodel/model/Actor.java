package pl.sda.filmappmodel.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "actor")
public class Actor {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "actor_id")
    private Integer id;

    @NotNull
    @Size(max = 50)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Size(max = 50)
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Past
    @Column(name = "birth_date")
    private Date birthDate;

    @NotNull
    @Past
    @Column(name = "passing_date")
    private Date passingDate;

    @DecimalMax(value = "10.0")
    @DecimalMin(value = "0.0")
    @Column(name = "rate")
    private double rating;

    @ManyToMany(mappedBy="actors")
    private Set<Film> films = new HashSet<>();

    public Actor() {
    }

    public Actor(String firstName, String lastName, Date birthDate, Date passingDate, double rating, Set<Film> films) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.passingDate = passingDate;
        this.rating = rating;
        this.films = films;
    }

    public Actor(int id, String firstName, String lastName, Date birthDate, Date passingDate, double rating) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.passingDate = passingDate;
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getPassingDate() {
        return passingDate;
    }

    public void setPassingDate(Date passingDate) {
        this.passingDate = passingDate;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Set<Film> getFilms() {
        return films;
    }

    public void setFilms(Set<Film> films) {
        this.films = films;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Actor actor = (Actor) o;

        if (Double.compare(actor.rating, rating) != 0) return false;
        if (id != null ? !id.equals(actor.id) : actor.id != null) return false;
        if (firstName != null ? !firstName.equals(actor.firstName) : actor.firstName != null) return false;
        return (lastName != null ? !lastName.equals(actor.lastName) : actor.lastName != null);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        temp = Double.doubleToLongBits(rating);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", passingDate=" + passingDate +
                ", rating=" + rating +
                '}';
    }
}
