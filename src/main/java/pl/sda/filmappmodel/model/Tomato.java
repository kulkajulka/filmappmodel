package pl.sda.filmappmodel.model;

import javax.persistence.Embeddable;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.util.Objects;

import static java.lang.Double.compare;

@Embeddable
public class Tomato {

    @DecimalMin(value = "0.0")
    @DecimalMax(value = "10.0")
    private Double userRating;

    @Min(0)
    private Integer userReviews;

    public Tomato() {
    }

    public Tomato(Double userRating, Integer userReviews) {
        this.userRating = userRating;
        this.userReviews = userReviews;
    }

    @Override
    public String toString() {
        return "Tomato{" +
                "userRating=" + userRating +
                ", userReviews=" + userReviews +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tomato tomato = (Tomato) o;

        return compare(tomato.userRating, userRating) == 0
                && Objects.equals(userReviews, tomato.userReviews);
    }

    @Override
    public int hashCode() {
        int result = userRating != null ? userRating.hashCode() : 0;
        result = 31 * result + (userReviews != null ? userReviews.hashCode() : 0);
        return result;
    }
}
